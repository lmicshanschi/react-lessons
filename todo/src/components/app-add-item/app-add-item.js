import React, { Component } from 'react';
import './app-add-item.css';

export default class AppAddItem extends Component {
  constructor() {
    super();
    this.state = {
      label: ''
    }
    this.onLabelChange = (e) => {
      this.setState ({
        label: e.target.value
      })
    }

    this.onSubmit = (e) => {
      e.preventDefault();
      this.props.addItem(this.state.label);
      this.setState({
        label: ''
      })
    }


  }
  render() {
  return(
    <form className = "app-add-item d-flex"
        onSubmit = { this.onSubmit }>
      <input type="text" className="form-control"
        onChange = { this.onLabelChange }
        placeholder = "What we should to done"
        value = { this.state.label }/>
      <button className = "btn btn-outline-secondary"
         onClick = { this.onSubmit  }>
              Add Item
        </button>
    </form>
  );
}
}

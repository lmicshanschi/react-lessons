import React from 'react';
import AppListItem from '../app-list-item/app-list-item';
import './app-list.css';

const AppList = ( { todos , onDeleted, onToggleImportant, onToggleDone } ) => {

  const elements = todos.map((item) => {
    const { id, ...restItems } = item;
   return (
     <li key = { id } className="list-group-item">
       <AppListItem { ...restItems }
       onDeleted = { () => onDeleted(id) }
       onToggleImportant = { () =>  onToggleImportant(id)}
       onToggleDone = {() => onToggleDone(id)}/>

     </li>
   );
 });


  return(
    <ul className="list-group todo-list">
    { elements }
  </ul>
  );
}

export default AppList;

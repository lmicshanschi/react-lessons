import React, { Component } from 'react';


import AppHeader from '../app-header';
import AppSearch from '../app-search';
import AppList from '../app-list';
import AppItemStatusFilter from '../app-item-status-filter';
import AppAddItem from '../app-add-item';
import  './app.css';


export default class App extends Component {

maxId = 100;

constructor () {
  super();
  this.state = {
    appData: [
      this.createTodoItem('Drink tea'),
      this.createTodoItem('Take a shower'),
      this.createTodoItem('Go to work')
    ]
  };



  this.deleteItem = (id) => {
    this.setState(({ appData }) => {
      const ind = appData.findIndex((el) => el.id === id);

      const before = appData.slice(0, ind);
      const after = appData.slice(ind + 1);

      const newArray = [ ...before, ...after];

      return {
        appData: newArray
      }

    })
  }

  this.addItem = (text) => {
    const newItem = this.createTodoItem(text);

    this.setState(({ appData })=>{
      const newArray = [ ...appData, newItem];

      return {
        appData: newArray
      }
    })
  }

  /*this.toggleProperty = (arr, id, propName) => {
    const ind = arr.findIndex((el) => el.id === id);

    const oldItem = arr[ind];

    const newItem = { ...oldItem, [propName]: !oldItem[propName]};

    const before = arr.slice(0, ind);
    const after = arr.slice(ind + 1);

    return {
      const newArray = [...before, newItem, ...after];
    }
  }*/

  this.onToggleImportant = (id) => {
    this.setState(({ appData }) => {
      const ind = appData.findIndex((el) => el.id === id);

      const oldItem = appData[ind];

      const newItem = { ...oldItem, important: !oldItem.important};

      const before = appData.slice(0, ind);
      const after = appData.slice(ind + 1);

      const newArray = [...before, newItem, ...after];

      return {
        appData: newArray
      }

    })
  }

  this.onToggleDone = (id) => {
    this.setState(({ appData }) => {
      const ind = appData.findIndex((el) => el.id === id);

      const oldItem = appData[ind];

      const newItem = { ...oldItem, done: !oldItem.done};

      const before = appData.slice(0, ind);
      const after = appData.slice(ind + 1);

      const newArray = [...before, newItem, ...after];

      return {
        appData: newArray
      }

    })
  }
}

createTodoItem(label) {
    return{
        label,
        important: false,
        done: false,
        id: this.maxId ++
    }
}

render() {
    const { appData } = this.state;
    const doneCount = appData.filter((el) => el.done).length;
    const todoCount = appData.length - doneCount;
  return(
    <div className="todo-app">
    <AppHeader toDo={ todoCount } done={ doneCount } />
    <div className="top-panel d-flex">
    <AppSearch />
    <AppItemStatusFilter />
    </div>
    <AppList todos = { appData }
    onDeleted = { this.deleteItem }
    onToggleImportant = { this.onToggleImportant }
    onToggleDone = { this.onToggleDone }
    />
    <AppAddItem addItem = { this.addItem }/>
    </div>
  );


}
}

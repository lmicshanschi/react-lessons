import React, { Component } from 'react';

import './app-item-status-filter.css';

export default class AppItemStatusFilter extends Component {
  render () {
    return (
      <div className="btn-group">
         <button type="button"
                 className="btn btn-info">All</button>
         <button type="button"
                 className="btn btn-outline-secondary">Active</button>
         <button type="button"
                 className="btn btn-outline-secondary">Done</button>
       </div>
    );
  }

}

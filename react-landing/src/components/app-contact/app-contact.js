import React from 'react';
import { Card, Container, Row, Col, Button, Form } from 'react-bootstrap';
import './app-contact.css';

const AppContact = () => {
  return (
    <Container className="contact-form">
    <h1 className="text-center">Contact with me</h1>
        <Form>
            <Form.Group controlId="exampleForm.ControlInput1">
                <Form.Label>Yor name:</Form.Label>
                <Form.Control type="text" placeholder="Please enter your name" />
            </Form.Group>

            <Form.Group controlId="exampleForm.ControlInput1">
                <Form.Label>Yor email:</Form.Label>
                <Form.Control type="email" placeholder="name@example.com" />
            </Form.Group>


            <Form.Group controlId="exampleForm.ControlTextarea1">
                <Form.Label>Example textarea</Form.Label>
                <Form.Control as="textarea" rows={3} />
            </Form.Group>
            <Button>Submit</Button>
    </Form>
   </Container>
  );
}

export default AppContact;

import React from 'react';
import { Jumbotron, Button, Container, Row, Col, Image } from 'react-bootstrap';

const AppJumbotron = () => {
  return (
    <Jumbotron>
    <Container className="mr-sm-4">
    <Row>
      <Col>
        <Image src="https://static.wikia.nocookie.net/rustarwars/images/d/d6/Yoda_SWSB.png/revision/latest?cb=20171222112613" height="300px" width="300px" roundedCircle />
      </Col>
        <Col>
      <h1>Hello, world!</h1>
      <p>
        This is a simple hero unit, a simple jumbotron-style component for calling
        extra attention to featured content or information.
      </p>
      <p>
        <Button variant="primary">About me</Button>
      </p>
      </Col>
    </Row>

  </Container>
</Jumbotron>
  );
}

export default AppJumbotron;

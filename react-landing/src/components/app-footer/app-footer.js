import React from 'react';
import { Container } from 'react-bootstrap';

import './app-footer.css';

const AppFooter = () => {
  return(
      <Container fluid className="app-footer">
        <Container className ="app-footer-content">
          <p>Our examen</p>
        </Container>
      </Container>
  );
}

export default AppFooter;

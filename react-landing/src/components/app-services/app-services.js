import React from 'react';
import { Card, Container, Row, Col, Button } from 'react-bootstrap';
import './app-services.css';

const AppServices = () => {
  return (
    <Container className ="app-service">
    <h1 className="text-center">My Services</h1>
      <Row>
          <Col>
              <Card className="text-center">
                  <Card.Header>Featured</Card.Header>
                  <Card.Body>
                    <Card.Title>Special title treatment</Card.Title>
                    <Card.Text>
                      With supporting text below as a natural lead-in to additional content.
                    </Card.Text>
                  </Card.Body>

              </Card>
          </Col>

          <Col>
              <Card className="text-center">
                  <Card.Header>Featured</Card.Header>
                  <Card.Body>
                    <Card.Title>Special title treatment</Card.Title>
                    <Card.Text>
                      With supporting text below as a natural lead-in to additional content.
                    </Card.Text>
                  </Card.Body>

              </Card>
          </Col>

          <Col>
              <Card className="text-center">
                  <Card.Header>Featured</Card.Header>
                  <Card.Body>
                    <Card.Title>Special title treatment</Card.Title>
                    <Card.Text>
                      With supporting text below as a natural lead-in to additional content.
                    </Card.Text>

                  </Card.Body>
              </Card>
          </Col>
      </Row>
    </Container>
  );
}

export default AppServices;

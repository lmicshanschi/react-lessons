import React from 'react';
import { Navbar, Container,  Nav, Form, FormControl, Button } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {  faFacebook, faInstagram, faVk } from '@fortawesome/free-brands-svg-icons';
import './app-header.css';

const AppHeader = () => {
  return(
    <Navbar collapseOnSelect expand="md" bg="dark" variant="dark">
      <Container>
        <Navbar.Brand>
          Exam
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="mr-auto">
            <Nav.Link href="/">Home</Nav.Link>
            <Nav.Link href="#">Services</Nav.Link>
            <Nav.Link href="#">Portfolio</Nav.Link>
            <Nav.Link href="#">Contact</Nav.Link>
          </Nav>
      <Nav.Link href="#"><FontAwesomeIcon icon={faFacebook} className = "social-icon" /></Nav.Link>
      <Nav.Link href="#"><FontAwesomeIcon icon={faInstagram} className = "social-icon"/></Nav.Link>
      <Nav.Link href="#"><FontAwesomeIcon icon={faVk} className = "social-icon"/></Nav.Link>


        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

export default AppHeader;

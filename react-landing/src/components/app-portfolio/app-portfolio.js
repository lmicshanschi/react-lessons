import React from 'react';
import { Card, Container, Row, Col, Button } from 'react-bootstrap';
import './app-portfolio.css';

const AppPortfolio = () => {
  return (

    <Container className="container-top">
    <h1 className="text-center">Portfolio</h1>
      <Row>
        <Col>
          <Card className="card-photo" style={{ width: '14rem' }}>
          <Card.Img variant="top" src="https://undark.org/wp-content/uploads/2020/02/GettyImages-1199242002-1-scaled.jpg" />
          </Card>
        </Col>
        <Col>
          <Card className="card-photo"  style={{ width: '14rem' }}>
          <Card.Img variant="top" src="https://undark.org/wp-content/uploads/2020/02/GettyImages-1199242002-1-scaled.jpg" />
          </Card>
        </Col>
        <Col>
          <Card className="card-photo"  style={{ width: '14rem' }}>
          <Card.Img variant="top" src="https://undark.org/wp-content/uploads/2020/02/GettyImages-1199242002-1-scaled.jpg" />
          </Card>
        </Col>
        <Col>
          <Card className="card-photo"  style={{ width: '14rem' }}>
          <Card.Img variant="top" src="https://undark.org/wp-content/uploads/2020/02/GettyImages-1199242002-1-scaled.jpg" />
          </Card>
        </Col>
        <Col>
          <Card className="card-photo"  style={{ width: '14rem' }}>
          <Card.Img variant="top" src="https://undark.org/wp-content/uploads/2020/02/GettyImages-1199242002-1-scaled.jpg" />
          </Card>
        </Col>
        <Col>
          <Card className="card-photo"  style={{ width: '14rem' }}>
          <Card.Img variant="top" src="https://undark.org/wp-content/uploads/2020/02/GettyImages-1199242002-1-scaled.jpg" />
          </Card>
        </Col>
        <Col>
          <Card className="card-photo"  style={{ width: '14rem' }}>
          <Card.Img variant="top" src="https://undark.org/wp-content/uploads/2020/02/GettyImages-1199242002-1-scaled.jpg" />
          </Card>
        </Col>
        <Col>
          <Card className="card-photo"  style={{ width: '14rem' }}>
          <Card.Img variant="top" src="https://undark.org/wp-content/uploads/2020/02/GettyImages-1199242002-1-scaled.jpg" />
          </Card>
        </Col>
      </Row>
   </Container>
  );
}

export default AppPortfolio;
